# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .timesheet import Timesheet
from .company import Employee, EmployeeCostPrice
from .team_timesheet import (TeamTimesheetEmployee, TeamTimesheetWork,
    EditEmployeeTimesheetsStart, CreateWorkTimesheetStart,
    EditEmployeeTimesheets, CreateWorkTimesheets)
from .profile import ProfilePayrollCost


def register():
    Pool.register(
        Timesheet,
        Employee,
        EmployeeCostPrice,
        module='timesheet_extra_time', type_='model')
    Pool.register(
        ProfilePayrollCost,
        module='timesheet_extra_time', type_='model',
        depends=['staff_profile'])
    Pool.register(
        TeamTimesheetWork,
        TeamTimesheetEmployee,
        EditEmployeeTimesheetsStart,
        CreateWorkTimesheetStart,
        module='timesheet_extra_time', type_='model',
        depends=['team_timesheet'])
    Pool.register(
        EditEmployeeTimesheets,
        CreateWorkTimesheets,
        module='timesheet_extra_time', type_='wizard',
        depends=['team_timesheet'])
