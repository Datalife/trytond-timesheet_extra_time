# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.transaction import Transaction
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Timesheet(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    type = fields.Selection([
            ('normal', 'Normal'),
            ('extra', 'Extra')
            ], 'Type', required=True)
    extra_duration = fields.Function(
        fields.TimeDelta('Extra duration'),
        'get_extra_duration', setter='set_extra_duration')

    @classmethod
    def __setup__(cls):
        super(Timesheet, cls).__setup__()
        cls.extra_duration._field.converter = cls.duration.converter
        if hasattr(cls, '_deny_modify_fields'):
            cls._deny_modify_fields.add('type')

    @fields.depends('extra_duration')
    def on_change_duration(self):
        try:
            super(Timesheet, self).on_change_duration()
        except AttributeError:
            pass

    @fields.depends('duration', 'extra_duration')
    def on_change_extra_duration(self):
        if hasattr(self, 'total_hours'):
            self.total_hours = self.get_total_hours()

    def get_total_hours(self, name=None):
        if (Transaction().context.get('edit_extra_duration', False) and
                self.extra_duration):
            return super(Timesheet, self).get_total_hours(name) + (
                self.extra_duration.total_seconds() / 3600)
        return super(Timesheet, self).get_total_hours(name)

    @classmethod
    def default_type(cls):
        return 'normal'

    @classmethod
    def _get_extra_duration_key(cls):
        res = ('date', 'employee', 'work')
        try:
            Pool().get('timesheet.team.timesheet')
        except KeyError:
            return res
        return res + ('tts_work', )

    def get_extra_duration(self, name=None):
        key = self._get_extra_duration_key()

        _domain = [('type', '=', 'extra')]
        for _field in key:
            _domain.append((_field, '=', getattr(self, _field)))

        lines = self.search(_domain, limit=1)
        if lines:
            return lines[0].duration
        return None

    @classmethod
    def set_extra_duration(cls, records, name, value):
        to_delete = []
        to_update = []

        key = cls._get_extra_duration_key()
        for record in records:
            _domain = [('type', '=', 'extra')]
            for _field in key:
                _domain.append((_field, '=', getattr(record, _field)))
            lines = cls.search(_domain, limit=1)
            if not value:
                to_delete.extend(lines)
            elif lines:
                to_update.extend([lines, {'duration': value}])
            else:
                cls.copy([record], default={
                    'type': 'extra',
                    'duration': value})

        if to_delete:
            cls.delete(to_delete)
        if to_update:
            cls.write(*to_update)

    @classmethod
    def validate(cls, records):
        super(Timesheet, cls).validate(records)

        ts = [r for r in records if r.type == 'extra']
        if not ts:
            return

        values = {}
        key = cls._get_extra_duration_key()
        for record in records:
            _domain = [('type', '=', 'extra')]
            for _field in key:
                _domain.append((_field, '=', getattr(record, _field)))
            record_key = (d[2] for d in _domain)
            if values.get(record_key):
                raise UserError(gettext(
                    'timesheet_extra_time.'
                    'msg_timesheet_line_duplicate_extra_duration',
                    line=record.rec_name))
            values.setdefault(record_key, _domain)

        # check in ddbb
        ts_ids = list(map(int, ts))
        for key, domain in values.items():
            if cls.search(domain + [('id', 'not in', ts_ids)]):
                raise UserError(gettext(
                    'timesheet_extra_time.'
                    'msg_timesheet_line_duplicate_extra_duration',
                    line=record.rec_name))

    @classmethod
    def view_attributes(cls):
        return [
            ('/tree/field[@name="extra_duration"]', 'tree_invisible',
                ~Eval('edit_extra_duration', False))]

    @classmethod
    def sync_cost(cls, lines):
        lines_normal = [l for l in lines if l.type == 'normal']
        lines_extra = [l for l in lines if l.type == 'extra']
        if lines_normal:
            super(Timesheet, cls).sync_cost(lines_normal)

        with Transaction().set_context(_check_access=False):
            to_write = []
            lines_extra = cls.browse(lines_extra)
            for line in lines_extra:
                extra_cost_price = line.employee.compute_extra_cost_price(
                    date=line.date)
                if extra_cost_price != line.cost_price:
                    to_write.extend([[line], {
                        'cost_price': extra_cost_price}])
            if to_write:
                cls.write(*to_write)
