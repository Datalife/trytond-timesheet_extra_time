==============
Team Timesheet
==============

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard
    >>> from datetime import timedelta
    >>> from trytond.modules.company_employee_team.tests.tools import create_team, get_team

Install team_timesheet Module::

    >>> config = activate_modules(['timesheet_extra_time', 'team_timesheet'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Get today date::

    >>> import datetime
    >>> today = datetime.date.today()

Check team timesheet::

    >>> _ = create_team()
    >>> team = get_team()
    >>> TeamTimesheet = Model.get('timesheet.team.timesheet')
    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts.save()
    >>> Work = Model.get('timesheet.work')
    >>> work1 = Work(name='Work1')
    >>> work1.save()
    >>> tts_work = tts.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.hours = timedelta(hours=8)
    >>> tts.save()
    >>> work, = [w for w in tts.works]
    >>> employee1 = tts.employees[0]
    >>> create_work_timesheets = Wizard('timesheet.team.timesheet-timesheet.work.create_timesheets', [work])
    >>> create_work_timesheets.form.creation_type = 'all_employees'
    >>> create_work_timesheets.form.hours.total_seconds() / 3600
    8.0
    >>> create_work_timesheets.execute('edit_timesheets')
    >>> tts = TeamTimesheet(tts.id)
    >>> len(tts.works[0].timesheets)
    1
    >>> employee, = [e for e in tts.employees]
    >>> edit_employee_timesheets = Wizard('timesheet.team.timesheet-company.employee.edit_timesheets', [employee])
    >>> edit_employee_timesheets.form.timesheets[0].extra_duration = timedelta(hours=2.5)
    >>> edit_employee_timesheets.execute('edit_timesheets')
    >>> tts.reload()
    >>> normal, = [t for t in tts.works[0].timesheets if t.type == 'normal']
    >>> normal.duration.total_seconds() / 3600
    8.0
    >>> extra, = [t for t in tts.works[0].timesheets if t.type == 'extra']
    >>> extra.duration.total_seconds() / 3600
    2.5
    >>> (extra.tts_work, extra.employee, extra.date, extra.work) == (
    ...         normal.tts_work, normal.employee, normal.date, normal.work)
    True

Modify one employee extra hours::

    >>> edit_employee_timesheets = Wizard('timesheet.team.timesheet-company.employee.edit_timesheets', [employee])
    >>> edit_employee_timesheets.form.timesheets[0].extra_duration = timedelta(hours=2)
    >>> edit_employee_timesheets.execute('edit_timesheets')
    >>> extra.reload()
    >>> extra.duration.total_seconds() / 3600
    2.0
    >>> edit_employee_timesheets = Wizard('timesheet.team.timesheet-company.employee.edit_timesheets', [employee])
    >>> edit_employee_timesheets.form.timesheets[0].extra_duration = timedelta(hours=0)
    >>> edit_employee_timesheets.execute('edit_timesheets')
    >>> tts.reload()
    >>> not [t for t in tts.works[0].timesheets if t.type == 'extra']
    True
    >>> create_work_timesheets = Wizard('timesheet.team.timesheet-timesheet.work.create_timesheets', [work])
    >>> create_work_timesheets.form.timesheets[0].total_hours
    8.0
    >>> config._context['edit_extra_duration'] = True
    >>> create_work_timesheets.form.timesheets[0].extra_duration = timedelta(hours=2.5)
    >>> create_work_timesheets.form.timesheets[0].total_hours
    10.5
    >>> create_work_timesheets.execute('edit_timesheets')
    >>> tts.reload()
    >>> not [t for t in tts.works[0].timesheets if t.type == 'extra']
    False
    >>> extra, = [t for t in tts.works[0].timesheets if t.type == 'extra']
    >>> extra.duration.total_seconds() / 3600
    2.5

Check line can not be modified when state is confirmed or done::

    >>> tts.click('wait')
    >>> tts.click('confirm')
    >>> tts.click('do')
    >>> tts.state
    'done'
    >>> tts.timesheets[0].type = 'extra'
    >>> tts.timesheets[0].save()
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: You cannot modify line "1" because its Team Timesheet "1" is in "Confirmed" or "Done" state. - 