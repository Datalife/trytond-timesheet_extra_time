# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.modules.timesheet_cost.company import price_digits


class ProfilePayrollCost(metaclass=PoolMeta):
    __name__ = 'staff.profile.payroll_price'

    extra_cost_price = fields.Numeric('Extra cost price',
        digits=price_digits)
