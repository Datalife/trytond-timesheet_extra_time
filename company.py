# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.modules.timesheet_cost.company import price_digits
import datetime


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    extra_cost_price = fields.Function(fields.Numeric('Extra cost price',
            digits=price_digits,
            help="Extra hourly cost price for this Employee"),
        'get_extra_cost_price')

    def get_extra_cost_price(self, name):
        ctx_date = Transaction().context.get('date', None)
        return self.compute_extra_cost_price(ctx_date)

    def get_employee_extra_costs(self):
        pool = Pool()
        CostPrice = pool.get('company.employee_cost_price')

        cost_prices = CostPrice.search([
                ('employee', '=', self.id),
                ], order=[('date', 'ASC')])

        employee_costs = []
        for cost_price in cost_prices:
            employee_costs.append(
                (cost_price.date, cost_price.extra_cost_price))
        return employee_costs

    def get_profile_extra_prices(self):
        pool = Pool()
        if not self.profile:
            return 0
        PayrollPrice = pool.get('staff.profile.payroll_price')
        payroll_prices = PayrollPrice.search([
            ('profile', '=', self.profile),
        ], order=[('date', 'ASC')])

        employee_costs = []
        for payroll_price in payroll_prices:
            employee_costs.append(
                (payroll_price.date, payroll_price.payroll_price,
                    payroll_price.extra_cost_price))
        return employee_costs

    def compute_extra_cost_price(self, date=None):
        pool = Pool()
        Date = pool.get('ir.date')

        cost = 0
        profile_price = 0
        profile_edate = datetime.date(1000, 1, 1)
        edate = datetime.date(1000, 1, 1)

        try:
            Profile = pool.get('staff.profile.payroll_price')
        except Exception:
            Profile = None
        if date is None:
            date = Date.today()

        # compute the cost price for the given date
        if Profile and self.profile:
            employee_prices = self.get_profile_extra_prices()
            if employee_prices and date >= employee_prices[0][0]:
                for profile_edate, _, profile_ecost_price in employee_prices:
                    if date >= profile_edate:
                        profile_price = profile_ecost_price
                    else:
                        break

        employee_costs = self.get_employee_extra_costs()
        if employee_costs and date >= employee_costs[0][0]:
            for edate, ecost in employee_costs:
                if date >= edate:
                    cost = ecost
                else:
                    break

        return (cost if edate >= profile_edate else profile_price) or 0


class EmployeeCostPrice(metaclass=PoolMeta):
    __name__ = 'company.employee_cost_price'

    extra_cost_price = fields.Numeric('Extra cost price',
        digits=price_digits, help="Extra hourly cost price")
