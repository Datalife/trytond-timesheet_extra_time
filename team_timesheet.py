# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from datetime import timedelta


class TeamTimesheetEmployee(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-company.employee'

    extra_hours = fields.Function(fields.TimeDelta('Extra Hours'),
        'get_extra_hours')

    @classmethod
    def __setup__(cls):
        super(TeamTimesheetEmployee, cls).__setup__()
        cls.extra_hours._field.converter = cls.hours._field.converter

    def get_extra_hours(self, name=None):
        lines = [t for t in self.timesheets if t.type == 'extra']
        if not lines:
            return None
        return timedelta(seconds=sum(
            t.duration.total_seconds() for t in lines))


class TeamTimesheetWork(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-timesheet.work'

    def _create_timesheets(self, creation_type, timesheets, hours,
            employees=[]):
        pool = Pool()
        Timesheet = pool.get('timesheet.line')

        normal_ts = [t for t in timesheets if t.type == 'normal']

        res = super()._create_timesheets(creation_type, normal_ts, hours,
            employees=employees)

        team_employees = self.team_timesheet.employees
        if not employees:
            employees = [e.employee for e in team_employees]
        # create extra hours timesheet lines
        for e in team_employees:
            if e.employee not in employees:
                continue
            if not e.extra_hours:
                continue
            if [t for t in e.timesheets if t.type == 'extra']:
                continue
            extra_ts = self._get_timesheet(Timesheet, e, e.extra_hours)
            if extra_ts:
                extra_ts.type = 'extra'
                res.append(extra_ts)

        return res


class EditTimesheetStartMixin(object):

    @classmethod
    def __setup__(cls):
        super(EditTimesheetStartMixin, cls).__setup__()
        cls.timesheets.domain.append(
            ('type', '=', 'normal'))
        cls.timesheets.context.update({'edit_extra_duration': True})


class EditEmployeeTimesheetsStart(EditTimesheetStartMixin, metaclass=PoolMeta):
    __name__ = \
        'timesheet.team.timesheet-company.employee.edit_timesheets.start'


class CreateWorkTimesheetStart(EditTimesheetStartMixin, metaclass=PoolMeta):
    __name__ = \
        'timesheet.team.timesheet-timesheet.work.create_timesheets.start'


class EditTimesheetMixin(object):

    def default_start(self, fields):
        pool = Pool()
        TtsEmployee = pool.get(Transaction().context['active_model'])

        res = super(EditTimesheetMixin, self).default_start(fields)
        tts_emp = TtsEmployee(Transaction().context['active_id'])
        res.update({
            'timesheets': [t.id for t in tts_emp.timesheets
                if t.type == 'normal']
            })
        return res

    def _delete(self):
        DetModel = Pool().get(Transaction().context['active_model'])
        record = DetModel(Transaction().context['active_id'])
        Timesheet = Pool().get('timesheet.line')

        Timesheet.delete([ts for ts in record.timesheets if
            ts not in self.start.timesheets and ts.type == 'normal'])


class EditEmployeeTimesheets(EditTimesheetMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-company.employee.edit_timesheets'


class CreateWorkTimesheets(EditTimesheetMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-timesheet.work.create_timesheets'
