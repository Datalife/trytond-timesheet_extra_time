datalife_timesheet_extra_time
=============================

The timesheet_extra_time module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-timesheet_extra_time/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-timesheet_extra_time)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
